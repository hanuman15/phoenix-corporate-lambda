package com.amazonaws.lambda.corporate.rest.util;

import java.io.DataOutputStream;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


public class ServiceCallHelper {
	
	public static byte[] downloadFileFromS3Server(String url) {

		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(60 * 1000)
				.setConnectTimeout(60 *1000).setConnectionRequestTimeout(60 *1000).build();

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				return EntityUtils.toByteArray(entity);
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			e.printStackTrace();
			//ElementLogger.log(ElementLoggerLevel.ERROR, "Could not download file from s3 server ", e,
			//ServiceCallHelper.class);
		}
		return null;
	}
	
	public static String[] getdownloadLinkFromS3Server(String url) {

		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10 * 1000)
				.setConnectTimeout(10 *1000).setConnectionRequestTimeout(10 *1000).build();
		StringBuilder responseStrBuilder = new StringBuilder();
		String[] serverResponse = new String[2];
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[0] = responseStrBuilder.toString();
			}else {
				serverResponse[0] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			/*
			 * ElementLogger.log(ElementLoggerLevel.ERROR,
			 * "Could not download file from s3 server ", e, ServiceCallHelper.class);
			 */
			e.printStackTrace();
		}
		return serverResponse;
	}
	
	public static String[] getCaseManagementS3LinkForUploadDoc(String url) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			//ElementLogger.log(ElementLoggerLevel.ERROR, "Could not update data on server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	
	public static int putCaseManagmentFileToS3Server(String url, File file) {
		int status =0;
		try {
			URL address = new URL(url);
			HttpURLConnection httpCon = (HttpURLConnection) address.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setRequestProperty("Content-Type", "binary/octet-stream" );
			httpCon.setRequestMethod("PUT");
			//httpCon.setRequestMethod("HEAD");

			byte[] bytes = Files.readAllBytes(file.toPath());
			DataOutputStream out = new DataOutputStream(httpCon.getOutputStream());
			out.write(bytes);
			out.flush();
			out.close();

			status = httpCon.getResponseCode();

		} catch (Exception e) {
			//ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post file to server ", e, ServiceCallHelper.class);
		} finally {
			if (file != null && file.exists())
				file.delete();
		}
		return status;
	}

	public static String[] getDataFromServerWithoutTimeoutWithApiKey(String url,String apiKey) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			if(apiKey!=null)
				httpGet.setHeader("x-api-key",apiKey);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			//ElementLogger.log(ElementLoggerLevel.ERROR, "Could not get data from server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	
	public static byte[] downloadFileFromServerWithoutTimeout(String url) {
		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			httpGet.setHeader(HttpHeaders.USER_AGENT,
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				return EntityUtils.toByteArray(entity);
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
		}
		return null;
	}
	
	public static byte[] downloadFileFromServerWithoutTimeoutWithApiKey(String url,String apiKey) {
		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			httpGet.setHeader(HttpHeaders.USER_AGENT,
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
			if(apiKey!=null)
				httpGet.setHeader("x-api-key",apiKey);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				return EntityUtils.toByteArray(entity);
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			//ElementLogger.log(ElementLoggerLevel.ERROR, "Could not download file from server ", e,
					//ServiceCallHelper.class);
		}
		return null;
	}
	public static String[] getDataFromServerWithoutTimeout(String url) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			//ElementLogger.log(ElementLoggerLevel.ERROR, "Could not get data from server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	public static String[] deleteDataInServerWithoutTimeout(String url) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpDelete httpDelete = new HttpDelete(url);
			response = httpClient.execute(httpDelete);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	
	public static String[] putStringDataToServerWithoutTimeout(String url, String jsonToBeSent) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpPut httpPut = new HttpPut(url);
			StringEntity stringEntity = new StringEntity(jsonToBeSent,"UTF-8");
			httpPut.setEntity(stringEntity);
			httpPut.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPut);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				System.out.println(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	
	public static String[] postStringDataToServer(String url, String jsonToBeSent) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10 * 1000)
				.setConnectTimeout(10 * 1000).setConnectionRequestTimeout(10 * 1000).build();

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			StringEntity stringEntity = new StringEntity(jsonToBeSent,"UTF-8");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	public static String[] postStringDataToServerWithoutTimeout(String url, String jsonToBeSent) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			StringEntity stringEntity = new StringEntity(jsonToBeSent,"UTF-8");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				System.out.println(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
}
