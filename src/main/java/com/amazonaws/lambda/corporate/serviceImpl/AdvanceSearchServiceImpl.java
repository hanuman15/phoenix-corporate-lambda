package com.amazonaws.lambda.corporate.serviceImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ResourceBundle;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

import com.amazonaws.lambda.corporate.dev.service.AdvanceSearchService;
import com.amazonaws.lambda.corporate.rest.util.ServiceCallHelper;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
@Service
public class AdvanceSearchServiceImpl implements AdvanceSearchService {


	ResourceBundle resource = ResourceBundle.getBundle("application");

	private String CORPORATE_STRUCTURE_LOCATION=/*resource.getString("corporate_structure_location")*/  System.getenv("CORPORATE_STRUCTURE_LOCATION");
	private String CORPORATE_BUCKET=System.getenv("CORPORATE_BUCKET");
	private String CLIENT_ID=/*resource.getString("client_id")*/ System.getenv("CLIENT_ID");
	
	@Override
	public String readCorporateStructure(String path) throws JSONException, Exception {
		
		System.out.println("inside in readCorporateStructure >>"+path);
		JSONObject response = new JSONObject();
		try {
			JSONParser parser = new JSONParser();
			File file = new File(CORPORATE_STRUCTURE_LOCATION + path + ".json");
			System.out.println("Going to download");
			byte[] bytes=downloadFileFromS3(path);
			
		//	byte[] bytes= downloadfile( path);
			
			System.out.println("Download completed");
	        try {
	 
	            OutputStream os = new FileOutputStream(file);
	            os.write(bytes);
	            System.out.println("Write bytes to file.");
	            os.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
			if (file.exists() && file.length() > 0) {
				System.out.println("file.exists()  and  size >>"+file.exists() +file.length() );
				FileReader reader = new FileReader(CORPORATE_STRUCTURE_LOCATION + path + ".json");
				Object obj = parser.parse(reader);
				org.json.simple.JSONArray mainResponse = (org.json.simple.JSONArray) obj;
				if(!mainResponse.isEmpty()){
					org.json.simple.JSONObject firstJson = (org.json.simple.JSONObject) mainResponse.get(0);
					//org.json.JSONObject firstJson = (org.json.JSONObject) mainResponse.get(0);
					//if(firstJson.containsKey("status") && "completed".equalsIgnoreCase(firstJson.get("status").toString())){
					if(firstJson.containsKey("status") && "completed".equalsIgnoreCase(firstJson.get("status").toString())){
						// Prateek
						//							Set<String> keys = firstJson.keySet();
						//							if(keys.size() == 2){
						//								response.put("status", "completed");
						//								response.put("data", mainResponse);
						//								return response.toString();
						//								
						//							}
						/// new
						if(firstJson.containsKey("error")) {
							response.put("status", "completed");
							response.put("data", mainResponse);
							return response.toString();
						}
						///

					}
					//return response.toString();
				}
				org.json.simple.JSONObject mainEntity = (org.json.simple.JSONObject) mainResponse.get(0);
				String mainEntityId = null;
				String mainEntityName = null;
				if (mainEntity.containsKey("identifier") && mainEntity.get("identifier") != null) {
					mainEntityId = (String) mainEntity.get("identifier").toString();
				}
				if (mainEntity.containsKey("name") && mainEntity.get("name") != null) {
					mainEntityName = (String) mainEntity.get("name").toString();
				}

				// fetching list of watch lists
				/*List<SignificantWatchList> significantWatchLists = significantWatchListService
							.fetchWtachListByEntityId(mainEntityId);
					for (int i = 0; i < mainResponse.size(); i++) {
						org.json.simple.JSONObject json = (org.json.simple.JSONObject) mainResponse.get(i);
						String identifier = null;
						String name = null;
						if (json.containsKey("identifier")) {
							if (json.get("identifier") != null)
								identifier = json.get("identifier").toString();
						}
						if (json.containsKey("name")) {
							if (json.get("name") != null)
								name = json.get("name").toString();
						}
						getSignificantPep(json, mainEntityId, mainEntityName, identifier, name, false,
								significantWatchLists);
						if (i == 0) {
							if (json.containsKey("officership")) {
								org.json.simple.JSONArray officership = (org.json.simple.JSONArray) json.get("officership");
								if (officership != null) {
									for (int j = 0; j < officership.size(); j++) {
										org.json.simple.JSONObject officer = (org.json.simple.JSONObject) officership
												.get(j);
										String officerName = null;
										if (officer.containsKey("name")) {
											if (officer.get("name") != null)
												officerName = officer.get("name").toString();
										}
										getSignificantPep(officer, mainEntityId, mainEntityName, officerName, officerName,
												true, significantWatchLists);
									}
								}
							}
						}
						if (json.containsKey("subsidiaries")) {
							org.json.simple.JSONArray subsidiaries = (org.json.simple.JSONArray) json.get("subsidiaries");
							if (subsidiaries != null) {
								for (int j = 0; j < subsidiaries.size(); j++) {
									org.json.simple.JSONObject subsidiarie = (org.json.simple.JSONObject) subsidiaries
											.get(j);
									String subsidiarieId = null;
									String subsidiarieName = null;
									if (subsidiarie.containsKey("identifier")) {
										if (subsidiarie.get("identifier") != null)
											subsidiarieId = subsidiarie.get("identifier").toString();
									}
									if (subsidiarie.containsKey("name")) {
										if (subsidiarie.get("name") != null)
											subsidiarieName = subsidiarie.get("name").toString();
									}
									getSignificantPep(subsidiarie, mainEntityId, mainEntityName, subsidiarieId,
											subsidiarieName, false, significantWatchLists);
								}
							}
						}
					}*/
				if (mainResponse.size() > 0) {
					org.json.simple.JSONObject lastObject = (org.json.simple.JSONObject) mainResponse
							.get(mainResponse.size() - 1);
					String status = (String) lastObject.get("status");
					if (status == null) {
						response.put("status", "inprogress");
					} else if(status.equals("error")) {
						mainResponse= new org.json.simple.JSONArray();
						response.put("status","completed");
					}else {
						response.put("status", status);
					}
					response.put("data", mainResponse);
					return response.toString();
				}
			} else {
				response.put("status", "inprogress");
				response.put("data", new JSONArray());
				return response.toString();

			}
		} catch (Exception e) {
			e.printStackTrace();
			response.put("status", "inprogress");
			response.put("data", new JSONArray());
			return response.toString();

		}
		
		return response.toString();

	}
	private byte[] downloadFileFromS3(String path) {
		byte[] byteArray=null;
		AmazonS3 s3client = AmazonS3ClientBuilder.defaultClient();
		String key=path+".json";
    	S3Object fileFromBucket = s3client.getObject(CORPORATE_BUCKET, key);
		 try {
			 byteArray = IOUtils.toByteArray(fileFromBucket.getObjectContent());
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return byteArray;
		
	}
	private byte[] downloadfile(String path) {
		byte[] fileData =null;
		if(path!=null) {
			System.out.println("Entered into download doc");
		//String s3DownloadLinkServerResponse[]= ServiceCallHelper.getdownloadLinkFromS3Server(BIG_DATA_S3_DOC_URL+"organizations/" + "de000a1ewww0" +"?fields=documents&client_id="+CLIENT_ID+"&doc_id="+path);
		String s3DownloadLinkServerResponse[]= ServiceCallHelper.getdownloadLinkFromS3Server("CASE_MANAGEMENT_BIG_DATA_S3_DOC_URL"+"document?client_id="+CLIENT_ID+"&doc_id="+path);
		if (s3DownloadLinkServerResponse[0] != null
		&& s3DownloadLinkServerResponse[0].trim().length() > 0) {
		JSONObject json = new JSONObject();
		json = new JSONObject(s3DownloadLinkServerResponse[0]);
		if(json.has("download_link") && json.has("doc_id") && json.getString("download_link") != null) {
		fileData = ServiceCallHelper.downloadFileFromS3Server(json.getString("download_link"));
		}
		}
		}
		if (fileData != null) {
		return fileData;
		} else {
		System.out.println("Failed to download a file from server");
		//throw new Exception(ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG);
		}
		return null;
		}
}

