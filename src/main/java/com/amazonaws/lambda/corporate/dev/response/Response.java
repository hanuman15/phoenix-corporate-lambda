package com.amazonaws.lambda.corporate.dev.response;

public class Response {
	 String data;
	 
	    public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Response [data=" + data + "]";
	}

}
