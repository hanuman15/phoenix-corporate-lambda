package com.amazonaws.lambda.corporate.dev.service;

import org.json.JSONException;

public interface AdvanceSearchService {

	String readCorporateStructure(String path) throws JSONException, Exception;
}
