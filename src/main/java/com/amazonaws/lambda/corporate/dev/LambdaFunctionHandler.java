package com.amazonaws.lambda.corporate.dev;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.amazonaws.lambda.corporate.dev.config.WebConfig;
import com.amazonaws.lambda.corporate.dev.controller.AdvanceSearchController;
import com.amazonaws.lambda.corporate.dev.dto.CorporateDto;
import com.amazonaws.lambda.corporate.dev.response.Response;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class LambdaFunctionHandler extends AbstractHandler<WebConfig>
		implements RequestStreamHandler/* <CorporateDto, Response> */ {
	HttpServletRequest request;
	static AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
			LambdaFunctionHandler.class.getPackage().getName());

	public LambdaFunctionHandler() {
		ctx.getAutowireCapableBeanFactory().autowireBean(this);
	}

	@Autowired
	AdvanceSearchController adc;

	@Override
	public void handleRequest(InputStream inputStream,OutputStream output, Context context) {

	
		CorporateDto inputDto= new CorporateDto();
		Response lambdaResponse = new Response();
		try {
			 JsonParser parser = new JsonParser();
		        InputStreamReader reader = new InputStreamReader(inputStream);
		        JsonElement element = parser.parse(reader);
		        if (element.isJsonObject()) {
		        	JsonObject jsonObj = element.getAsJsonObject();
		      
		        	if(jsonObj.has("body")) {
		        		JSONParser parser2 = new JSONParser();
		        		JSONObject json = (JSONObject) parser2.parse(jsonObj.get("body").getAsString());
		        		
		        		ObjectMapper mapper = new ObjectMapper();
		        		//Convert JSON to POJO
		        		inputDto=mapper.readValue(json.toJSONString(), CorporateDto.class);
		        		if(inputDto.getPath()!=null) {
		        			System.out.println("ownershipDto.getIdentifier()>> "+ inputDto.getPath());
		        		}
		        	}
		        }
		
			String data = adc.getOwnershipPath(inputDto.getPath(), request);
			lambdaResponse.setData(data.toString());
			System.out.println("Parsing the Data after getting getCorporate");
			JSONParser parser2 = new JSONParser();
    		JSONObject json = (JSONObject) parser2.parse(data);
			org.json.JSONObject responseJson= new org.json.JSONObject();
			org.json.JSONObject datJson= new org.json.JSONObject(json.toJSONString());
			context.getLogger().log(" JSON Parsed Response : " );
			// responseJson to be returned to API gateway.
			responseJson.put("statusCode", 200);
			 responseJson.put("body", datJson.toString());
			 // Writing the response to output Stream 
			OutputStreamWriter writer = new OutputStreamWriter(output, "UTF-8");
			writer.write(responseJson.toString());
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
			lambdaResponse.setData(e.getMessage());
		}
		context.getLogger().log("Response : " + lambdaResponse);
	}
}


