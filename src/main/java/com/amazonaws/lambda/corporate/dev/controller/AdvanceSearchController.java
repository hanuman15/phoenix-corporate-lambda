package com.amazonaws.lambda.corporate.dev.controller;


import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
/*
import element.bst.elementexploration.rest.extention.advancesearch.dto.HierarchyDto;
import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService;
import element.bst.elementexploration.rest.extention.sourcecredibility.cloudDto.SourcesPaginationDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.cloudservice.ClassificationCloudService;
import element.bst.elementexploration.rest.extention.sourcecredibility.cloudservice.SourceCloudService;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.ClassificationsDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto;*/

import com.amazonaws.lambda.corporate.dev.service.AdvanceSearchService;

/*import com.amazonaws.lambda.ownership.advancesearch.dto.HierarchyDto;
import com.amazonaws.lambda.ownership.advancesearch.service.AdvanceSearchService;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudDto.SourcesPaginationDto;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudservice.ClassificationCloudService;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudservice.SourceCloudService;
import com.amazonaws.lambda.ownership.sourcecredibility.dto.ClassificationsDto;
import com.amazonaws.lambda.ownership.sourcecredibility.dto.SourcesDto;
*/
/**
 * 
 * @author Viswanath Reddy G
 *
 */

@RestController
@Component
@RequestMapping("/api/advancesearch")
//@PropertySource(value = "classpath:application.properties")
public class AdvanceSearchController {


	
	@Autowired
	AdvanceSearchService advanceSearchService;
	
	private String CORPORATE_STRUCTURE_LOCATION ;
	
	  @GetMapping(value = "/getCorporateStructure", produces = { "application/json; charset=UTF-8" }) 
	  public String	  getOwnershipPath(@RequestParam String path, HttpServletRequest request) 
			  throws Exception {
	  System.out.println("Entered into /getCorporateStructure controller");
	  return (advanceSearchService.readCorporateStructure(path));
	  }
	 

}

