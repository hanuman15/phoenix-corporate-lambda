package com.amazonaws.lambda.corporate.dev.dto;

import java.io.Serializable;

public class CorporateDto  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2037611944245893195L;
	
	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	
}
